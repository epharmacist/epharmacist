const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config({path:'./config.env'})

const app = require('./app')


const DB = "mongodb://localhost:27017/epharmacist"


// process.env.DTABASE.replace(
//     'PASSWORD',
//     process.env.DATABASE_PASSWORD,

// )

//console.log(process.env.DATABASE_PASSWORD)
mongoose.connect(DB).then((con) => {
    console.log(con.connections)
    console.log('DB connection successful')

}).catch(error=> console.log(error));

/*starting the server on port 4002.*/

const port = 4001
app.listen(port, () => console.log(`Server is running on ${port} port`))

