const express = require('express')
const router=express.Router()
const viewController=require('./../controller/viewController')
const authController = require('./../controller/authController')
router.get('/',viewController.getHome)
router.get('/login',viewController.getLoginForm)
router.get('/signup',viewController.getSignupForm)
router.get('/me', authController.protect, viewController.getProfile)


module.exports=router