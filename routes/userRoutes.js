const express = require('express')
const userControllers = require('./../controller/userController')
const authController = require('./../controller/authController')
const router = express.Router()


router.post('/signup',authController.signup)
router.post('/login', authController.login)
router.get('/logout', authController.logout)
router
    .route('/')
    .get(userControllers.getAllUser)
    .post(userControllers.createUser)

router
    .route('/:id')
    .get(userControllers.getUser)
    .patch(userControllers.updateUser)
    .delete(userControllers.deleteUser)

module.exports=router
